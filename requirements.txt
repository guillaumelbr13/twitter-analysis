ipykernel==5.1.3
mysql-connector==2.2.9
textblob==0.15.3
textblob-fr==0.2.0
tweepy==3.8.0
pandas==0.25.3
plotly==4.4.1
