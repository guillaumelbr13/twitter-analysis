TRACK_WORDS = ['retraites', 'grèves']
TABLE_NAME = "retraites"
TABLE_ATTRIBUTES = """tweet VARCHAR(255), polarity INT, subjectivity INT,
                      id_tweet VARCHAR(255), created_at TIMESTAMP, user_created_at VARCHAR(255),
                      user_location VARCHAR(255), user_description VARCHAR(255),
                      user_followers_count INT, retweet_count INT, favorite_count INT"""

NEUTRAL_COLOR = '#B2D732'
NEGATIVE_COLOR = '#347B98'
POSITIVE_COLOR = '#66B032'
BAR_COLOR = '#092834'

GRAPH_INTERVAL = 1*10000 # in milliseconds