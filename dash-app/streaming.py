# Common imports
import os
import re
import datetime
import pandas as pd

# Tweepy and Sentiment Analysis
import tweepy
from textblob import Blobber
from textblob_fr import PatternTagger, PatternAnalyzer

# PostgreSQL
import psycopg2

# App imports
import credentials
import settings


class TwitterStreamListener(tweepy.StreamListener):
    
    def on_status(self, status):
        """
        Extracts and exports tweets from Twitter to Heroku PostgreSQL database
        """
        
        # Ignore retweets
        if status.retweeted:
            return True
        
        # Extracts and analyses tweet
        tweet = clean_tweet(status.text)
        tb = Blobber(pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
        sentiment = tb(tweet).sentiment
        polarity = sentiment[0]
        subjectivity = sentiment[1]
        
        # More info about the tweet
        id_tweet = status.id_str
        created_at = status.created_at + datetime.timedelta(hours=1)
        user_created_at = status.user.created_at
        user_location = clean_infos(status.user.location)
        user_description = clean_infos(status.user.description)
        user_followers_count = status.user.followers_count
        retweet_count = status.retweet_count
        favorite_count = status.favorite_count
        
        # Check the stream
        print(status.text)
        
        # Store data in Heroku PostgreSQL
        cursor = connection.cursor()
        sql_request = """INSERT INTO {} (tweet, polarity, subjectivity, id_tweet, created_at, user_created_at,
                            user_location, user_description, user_followers_count, retweet_count, favorite_count) VALUES
                            (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""".format(settings.TABLE_NAME)
        
        values = tweet, polarity, subjectivity, id_tweet, created_at, user_created_at, user_location, \
                    user_description, user_followers_count, retweet_count, favorite_count
        
        cursor.execute(sql_request, values)
        connection.commit()

        delete_query =  '''
        DELETE FROM {}
        WHERE id_tweet IN (
            SELECT id_tweet
            FROM {}
            ORDER BY created_at asc
            LIMIT 200) AND (SELECT COUNT(*) FROM {}) > 9600;
        '''.format(settings.TABLE_NAME, settings.TABLE_NAME, settings.TABLE_NAME)
        
        cursor.execute(delete_query)
        connection.commit()
        cursor.close()           
        
        
    def on_error(self, status_code):
        """
        Stop scrapping data if it exceeds the Twitter API rate
        """
        if status_code == 420:
            return False


def clean_tweet(tweet): 
    """
    Removes links, address and special characters
    """
    tw = ' '.join(re.sub("(@[A-Za-z0-9]+)|(\w+:\/\/\S+)", " ", tweet).split())
    return tw.encode('ascii', 'ignore').decode('ascii')

def clean_infos(text):
    """
    Removes ASCII characters
    """
    if text:
        return text.encode('ascii', 'ignore').decode('ascii')
    return None


# Connection to Heroku PostgreSQL database
DATABASE_URL = os.environ['DATABASE_URL']
connection = psycopg2.connect(DATABASE_URL, sslmode='require')
cursor = connection.cursor()

# If the table doesn't exist, create one
cursor.execute("""SELECT COUNT(*)
                  FROM information_schema.tables 
                  WHERE table_name = '{}';""".format(settings.TABLE_NAME))
    
if cursor.fetchone()[0] != 1:
    cursor.execute("CREATE TABLE {} ({})".format(settings.TABLE_NAME, settings.TABLE_ATTRIBUTES))
    connection.commit()

cursor.close()

# Connection to Twitter API thanks to tweepy and keys from Twitter Developer Account
auth  = tweepy.OAuthHandler(credentials.API_KEY, credentials.API_KEY_SECRET)
auth.set_access_token(credentials.ACCESS_TOKEN,credentials.ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)

# Start the streaming and the collection of data from Twitter
Stream = tweepy.Stream(auth=api.auth, listener=TwitterStreamListener())
Stream.filter(track=settings.TRACK_WORDS)

# Close the connection (won't be reached)
connection.close()
